import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Message } from 'primeng/api';
import { switchMap, take } from 'rxjs';
import { PatientInput } from 'src/app/inputs/patient.input';
import { PatientCreateInput } from 'src/app/inputs/patient-create.input';
import { Patient } from 'src/app/responses/patient.response';
import { CreatePatientService } from 'src/app/service/create-patient.service';
import { ListByIdPatientService } from 'src/app/service/list-by-id-patient.service';
import { ListPatientsService } from 'src/app/service/list-patients.service';
import { UpdatePatientService } from 'src/app/service/update-patient.service';

@Component({
  selector: 'app-patient-edition',
  templateUrl: './patient-edition.component.html',
  styleUrls: ['./patient-edition.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PatientEditionComponent implements AfterViewInit {
  public edit: boolean;
  public form: FormGroup;
  public id: number;

  constructor(
    private _listPatientByIdService: ListByIdPatientService,
    private _createPatientService: CreatePatientService,
    private _updatePatientService: UpdatePatientService,
    private _listPatientsService: ListPatientsService,
    private _cdr: ChangeDetectorRef,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.form = new FormGroup({
      patientId: new FormControl(0),
      names: new FormControl(''),
      lastName: new FormControl(''),
      dni: new FormControl(''),
      address: new FormControl(''),
      email: new FormControl(''),
      phone: new FormControl(''),
    });
    this.edit = false;
    this.id = 0;
  }

  public ngAfterViewInit(): void {
    this._initialize();
  }

  private _initialize(): void {
    this._route.params.pipe(take(1)).subscribe((data) => {
      this.id = data['id'];
      this.edit = data['id'] != null;
      this._initForm();
    });
  }

  private _initForm(): void {
    if (this.edit) {
      this._listPatientByIdService
        .listById(this.id)
        .subscribe((patient: Patient) => {
          this.form = new FormGroup({
            patientId: new FormControl(patient.patientId),
            names: new FormControl(patient.names),
            lastName: new FormControl(patient.lastName),
            dni: new FormControl(patient.dni),
            address: new FormControl(patient.address),
            email: new FormControl(patient.email),
            phone: new FormControl(patient.phone),
          });
          this._cdr.markForCheck();
        });
    }
  }

  public operate(): void {
    this.edit ? this._updatePatient() : this._createPatient();
    this._router.navigate(['patients']);
  }

  private _createPatient(): void {
    this._createPatientService
      .create(this._instanceCreatePatient())
      .pipe(take(1))
      .subscribe((patient: Patient) => {
        const message: Message = {
          severity: 'success',
          summary: 'Success',
          detail: `Se registro el paciente su Id es: ${patient.patientId}`,
        };
        this._listPatientsService.setChangeMessage(message);
        this._listPatientsService
          .list()
          .pipe(take(1))
          .subscribe((patients: Patient[]) => {
            this._listPatientsService.setChangePatient(patients);
          });
      });
  }

  private _updatePatient(): void {
    this._updatePatientService
      .update(this._instanceUpdatePatient())
      .pipe(
        switchMap((patient: Patient) => {
          const message: Message = {
            severity: 'success',
            summary: 'Success',
            detail: `Se actualizo el paciente  con Id: ${patient.patientId}`,
          };
          this._listPatientsService.setChangeMessage(message);
          return this._listPatientsService.list();
        }),
        take(1)
      )
      .subscribe((patients: Patient[]) => {
        this._listPatientsService.setChangePatient(patients);
      });
  }

  private _instanceCreatePatient(): PatientCreateInput {
    let patient: PatientCreateInput = {
      names: this.form.value['names'],
      lastName: this.form.value['lastName'],
      dni: this.form.value['dni'],
      phone: this.form.value['phone'],
      address: this.form.value['address'],
      email: this.form.value['email'],
    };
    return patient;
  }

  private _instanceUpdatePatient(): PatientInput {
    let patient: PatientInput = {
      patientId: this.form.value['patientId'],
      names: this.form.value['names'],
      lastName: this.form.value['lastName'],
      dni: this.form.value['dni'],
      phone: this.form.value['phone'],
      address: this.form.value['address'],
      email: this.form.value['email'],
    };
    return patient;
  }
}
