import { ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message, MessageService } from 'primeng/api';
import { Subject, takeUntil } from 'rxjs';
import { take } from 'rxjs/internal/operators/take';
import { Patient } from 'src/app/responses/patient.response';
import { DeleteByIdPatientService } from 'src/app/service/delete-by-id-patient.service';
import { ListPatientsService } from 'src/app/service/list-patients.service';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css'],
  providers: [MessageService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
// LEER ESTRATEGIAS DE DETECCION DE CAMBIOS Y CICLO DE VIDA DEL COMPONENTE
export class PatientComponent implements OnInit, OnDestroy {
  public dataTable: Patient[];
  public first: number;
  public loading: boolean;
  public msgs: Message[];

  private _unSubscribeSubject: Subject<void>;

  constructor(
    private _deleteByIdPatientService: DeleteByIdPatientService,
    private _listPatientsService: ListPatientsService,
    private _cdr: ChangeDetectorRef,
    private _route: ActivatedRoute,
  ) {
    this._unSubscribeSubject = new Subject<void>();
    this.dataTable = [];
    this.loading = true;
    this.first = 0;
    this.msgs = [];
  }

  public ngOnInit(): void {
    this._initialize();
  }

  public ngOnDestroy(): void {
    this._finalize();
  }

  public applyFilterGlobal(event: any): string {
    return event.target.value;
  }

  public deletePatient(id: number): void {
    this._deleteByIdPatientService
      .delete(id)
      .pipe(take(1))
      .subscribe(() => {
        this._listPatientsService.list().subscribe((patients: Patient[]) => {
          this.dataTable = patients;
          this._cdr.markForCheck();
        });
      });
  }

  public isChildren() {
    return this._route.children.length > 0;
  }

  private _initialize(): void {
    this._loadPatients();
    this._watchChangePatients();
    this._watchChangeMessage();
    this.loading = false;
  }

  private _finalize(): void {
    this._unSubscribeSubject.next();
    this._unSubscribeSubject.complete();
  }

  private _loadPatients(): void {
    this._listPatientsService
      .list()
      .pipe(take(1))
      .subscribe((patients: Patient[]) => {
        this._createTable(patients);
      });
  }

  private _watchChangePatients(): void {
    this._listPatientsService
      .getChangePatient()
      .pipe(takeUntil(this._unSubscribeSubject))
      .subscribe((patients: Patient[]) => {
        this._createTable(patients);
      });
  }

  private _watchChangeMessage(): void {
    this._listPatientsService
      .getChangeMessage()
      .pipe(takeUntil(this._unSubscribeSubject))
      .subscribe((message: Message) => {
        this.msgs = [message];
        this._cdr.markForCheck();
      });
  }

  private _createTable(patients: Patient[]): void {
    this.dataTable = patients;
    this._cdr.markForCheck();
  }
}
