import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message, MessageService } from 'primeng/api';
import { Subject, switchMap, take, takeUntil } from 'rxjs';
import { Exam } from 'src/app/responses/exam.response';
import { DeleteByIdExamService} from 'src/app/service/delete-by-id-exams.service';
import { ListExamsService } from 'src/app/service/list-exams.service';

@Component({
  providers: [MessageService],
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExamComponent implements OnInit, OnDestroy {
  public columns: any[];
  public data: Exam[];
  public first: number;
  public loading: boolean;

  private _unSubscribeSubject: Subject<void>;

  constructor(
    private _deleteExamService: DeleteByIdExamService,
    private _listExamsService: ListExamsService,
    private _messageService: MessageService,
    private _cdr: ChangeDetectorRef,
    private _route: ActivatedRoute,
  ) {
    this.columns = [
      { field: 'examId', header: 'ExamID', align: 'center' },
      { field: 'name', header: 'Name', align: 'center' },
      { field: 'description', header: 'Description', align: 'center' },
      { field: 'actions', header: 'Actions' },
    ];
    this._unSubscribeSubject = new Subject<void>();
    this.loading = true;
    this.first = 0;
    this.data = [];
  }

  public ngOnInit() {
    this._initialize();
  }

  public ngOnDestroy(): void {
    this._finalize();
  }

  public deleteExam(id: number): void {
    this._deleteExamService
      .delete(id)
      .pipe(
        switchMap(() => {
          let message = {
            severity: 'success',
            summary: 'Deleted successfully',
            detail: `Exam removed with id: ${id}`,
          };
          this._createToast(message);
          return this._listExamsService.list();
        }), take(1)
      )
      .subscribe((exams: Exam[]) => {
        this._createTable(exams);
      });
  }

  public isChildren(): boolean {
    return this._route.children.length !== 0;
  }

  public applyFilterGlobal(event: any): string {
    return event.target.value;
  }

  private _initialize(): void {
    this._loadExams();
    this._watchChangeExams();
    this._watchChangeMessage();
    this.loading = false;
  }

  private _finalize(): void {
    this._unSubscribeSubject.next();
    this._unSubscribeSubject.complete();
  }

  private _loadExams(): void {
    this._listExamsService
      .list()
      .pipe(take(1))
      .subscribe((exams: Exam[]) => {
        this._createTable(exams);
      });
  }

  private _watchChangeExams(): void {
    this._listExamsService
      .getchangeExam()
      .pipe(takeUntil(this._unSubscribeSubject))
      .subscribe((exams: Exam[]) => {
        this._createTable(exams);
      });
  }

  private _watchChangeMessage(): void {
    this._listExamsService
      .getchangeMessage()
      .pipe(takeUntil(this._unSubscribeSubject))
      .subscribe((message: Message) => {
        this._createToast(message);
      });
  }

  private _createToast(message: Message): void {
    this._messageService.add(message);
    this._cdr.markForCheck();
  }

  private _createTable(exams: Exam[]): void {
    this.data = exams;
    this._cdr.markForCheck();
  }
}
