import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Message } from 'primeng/api';
import { switchMap, take } from 'rxjs';
import { ExamInput } from 'src/app/inputs/exam.input';
import { ExamCreateInput } from 'src/app/inputs/exam-create.input';
import { Exam } from 'src/app/responses/exam.response';
import { CreateExamService } from 'src/app/service/create-exam.service';
import { ListByIdExamsService } from 'src/app/service/list-by-id-exams.service';
import { ListExamsService } from 'src/app/service/list-exams.service';
import { UpdateExamService } from 'src/app/service/update-exam.service';

@Component({
  selector: 'app-exam-edition',
  templateUrl: './exam-edition.component.html',
  styleUrls: ['./exam-edition.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExamEditionComponent implements AfterViewInit {
  public form: FormGroup;
  public id: number;
  public edit: boolean;

  constructor(
    private _listByIdExamService: ListByIdExamsService,
    private _createExamService: CreateExamService,
    private _updateExamService: UpdateExamService,
    private _listExamService: ListExamsService,
    private _crd: ChangeDetectorRef,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.id = 0;
    this.edit = false;
    this.form = new FormGroup({
      examId: new FormControl(0),
      name: new FormControl(''),
      description: new FormControl(''),
    });
  }

  public ngAfterViewInit(): void {
    this._initialize();
  }

  public operate(): void {
    this.edit ? this._examUpdate() : this._examCreate();
    this._router.navigate(['exams']);
  }

  private _initialize(): void {
    this._route.params.pipe(take(1)).subscribe((data) => {
      (this.id = data['id']), (this.edit = data['id'] != null);
      this._initForm();
    });
  }

  private _initForm(): void {
    if (this.edit) {
      this._listByIdExamService.listById(this.id).subscribe((exam: Exam) => {
        this._loadForm(exam);
      });
    }
  }

  private _loadForm(exam: Exam): void {
    this.form = new FormGroup({
      examId: new FormControl(exam.examId),
      name: new FormControl(exam.name),
      description: new FormControl(exam.description),
    });
    this._crd.markForCheck();
  }

  private _examUpdate(): void {
    let instance: ExamInput = this._instanceExamUpdate();
    this._updateExamService
      .update(instance)
      .pipe(
        switchMap((exam: Exam) => {
          const message: Message = {
            severity: 'success',
            summary: 'Success',
            detail: `Exam updated with exam Id: ${exam.examId}`,
          };
          this._listExamService.setChangeMessage(message);
          return this._listExamService.list();
        })
      )
      .subscribe((exams: Exam[]) => {
        this._listExamService.setChangeExams(exams);
      });
  }

  private _examCreate(): void {
    let instance: ExamCreateInput = this._instanceExamCreate();
    this._createExamService
      .create(instance)
      .pipe(
        switchMap((exam: Exam) => {
          const message: Message = {
            severity: 'success',
            summary: 'Success',
            detail: `Exam created with examId: ${exam.examId}`,
          };
          this._listExamService.setChangeMessage(message);
          return this._listExamService.list();
        }),
        take(1)
      )
      .subscribe((exams: Exam[]) => {
        this._listExamService.setChangeExams(exams);
      });
  }

  private _instanceExamUpdate(): ExamInput {
    let instance: ExamInput = {
      examId: this.form.value['examId'],
      name: this.form.value['name'],
      description: this.form.value['description'],
    };
    return instance;
  }

  private _instanceExamCreate(): ExamCreateInput {
    let instance: ExamCreateInput = {
      name: this.form.value['name'],
      description: this.form.value['description'],
    };
    return instance;
  }
}
