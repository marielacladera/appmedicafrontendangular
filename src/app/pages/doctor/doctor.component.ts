import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Message, MessageService } from 'primeng/api';
import { Subject, switchMap, take, takeUntil } from 'rxjs';
import { Doctor } from 'src/app/responses/doctor.response';
import { DeleteByIdDoctorService } from 'src/app/service/delete-by-id-doctor.service';
import { ListDoctorsService } from 'src/app/service/list-doctors.service';

@Component({
  providers: [MessageService],
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DoctorComponent implements OnInit, OnDestroy {
  public dataTable: Doctor[];
  public displayModal: boolean;
  public doctor: Doctor;
  public first: number;
  public loading: boolean;
  public title: string;

  private readonly _EMPTY: string = '';
  private readonly _NUMBER_EMPTY: number = 0;
  private unSubscribeSubject: Subject<void>;

  constructor(
    private _deleteDoctorService: DeleteByIdDoctorService,
    private _listDoctorService: ListDoctorsService,
    private _messageService: MessageService,
    private _cdr: ChangeDetectorRef
  ) {
    this.doctor = {
      doctorId: this._NUMBER_EMPTY,
      names: this._EMPTY,
      lastName: this._EMPTY,
      cmp: this._EMPTY,
      urlPhoto: this._EMPTY,
    };
    this.unSubscribeSubject = new Subject<void>();
    this.title = 'Create new doctor';
    this.displayModal = false;
    this.loading = true;
    this.dataTable = [];
    this.first = 0;
  }

  public ngOnInit(): void {
    this._initialize();
  }

  public ngOnDestroy(): void {
    this._finalize();
  }

  public applyFilterGlobal(event: any): string {
    return event.target.value;
  }

  public showDialog(doctor?: Doctor): void {
    if (doctor && doctor.doctorId > 0) {
      this.doctor = { ...doctor };
      this.title = 'Update Doctor';
    } else {
      this.title = 'Create new doctor';
      this._changeDoctorData();
    }
    this.displayModal = true;
    this._cdr.markForCheck();
  }

  public deleteDoctor(id: number): void {
    this._deleteDoctorService
      .delete(id)
      .pipe(
        switchMap(() => {
          return this._listDoctorService.list();
        }),
        take(1)
      )
      .subscribe((doctors: Doctor[]) => {
        this._createTable(doctors);
      });
  }

  public cancelModalDoctor(estate: boolean): void {
    this.displayModal = estate;
    this._cdr.markForCheck();
  }

  private _changeDoctorData(): void {
    this.doctor = {
      doctorId: this._NUMBER_EMPTY,
      names: this._EMPTY,
      lastName: this._EMPTY,
      cmp: this._EMPTY,
      urlPhoto: this._EMPTY,
    };
  }

  private _initialize(): void {
    this._loadDoctors();
    this._watchChangeDoctors();
    this._watchChangeMessage();
    this.loading = false;
  }

  private _finalize(): void {
    this.unSubscribeSubject.next();
    this.unSubscribeSubject.complete();
  }

  private _loadDoctors(): void {
    this._listDoctorService
      .list()
      .pipe(take(1))
      .subscribe((doctors: Doctor[]) => {
        this._createTable(doctors);
      });
  }

  private _createTable(doctors: Doctor[]): void {
    this.dataTable = doctors;
    this._cdr.markForCheck();
  }

  private _watchChangeDoctors(): void {
    this._listDoctorService
      .getChangeDoctor()
      .pipe(takeUntil(this.unSubscribeSubject))
      .subscribe((doctors: Doctor[]) => {
        this._createTable(doctors);
      });
  }

  private _watchChangeMessage(): void {
    this._listDoctorService
      .getChangeMessage()
      .pipe(takeUntil(this.unSubscribeSubject))
      .subscribe((message: Message) => {
        this._messageService.add(message);
        this._cdr.markForCheck();
      });
  }
}
