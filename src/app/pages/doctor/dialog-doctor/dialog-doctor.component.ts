import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Message } from 'primeng/api';
import { switchMap } from 'rxjs';
import { DoctorInput } from 'src/app/inputs/doctor.input';
import { DoctorCreateInput } from 'src/app/inputs/doctor-create.input';
import { Doctor } from 'src/app/responses/doctor.response';
import { CreateDoctorService } from 'src/app/service/create-doctor.service';
import { ListDoctorsService } from 'src/app/service/list-doctors.service';
import { UpdateDoctorService } from 'src/app/service/update-doctor.service';

@Component({
  selector: 'app-dialog-doctor',
  templateUrl: './dialog-doctor.component.html',
  styleUrls: ['./dialog-doctor.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogDoctorComponent {
  @Input() public doctor!: Doctor;
  @Output() emisor: EventEmitter<boolean>;

  constructor(
    private _createDoctorService: CreateDoctorService,
    private _updateDoctorService: UpdateDoctorService,
    private _listDoctorService: ListDoctorsService
  ) {
    this.emisor = new EventEmitter();
  }

  public operation(): void {
    this.doctor.doctorId > 0? this._updateDoctor(): this._createDoctor();
    this.cancel();
  }

  public cancel(): void {
    this.emisor.emit(false);
  }

  private _createDoctor(): void {
    this._createDoctorService
      .create(this._instanceCreateDoctor())
      .pipe(
        switchMap((d: Doctor) => {
          const message: Message = {
            severity: 'success',
            summary: 'Success',
            detail: `Doctor created with doctorId: ${d.doctorId}`,
          };
          this._listDoctorService.setChangeMessage(message);
          return this._listDoctorService.list();
        })
      )
      .subscribe((doctors: Doctor[]) => {
        this._listDoctorService.setChangeDoctor(doctors);
      });
  }

  private _updateDoctor(): void {
    this._updateDoctorService
      .update(this._instanceUpdateDoctor())
      .pipe(
        switchMap((doctor: Doctor) => {
          const message: Message = {
            severity: 'success',
            summary: 'Success',
            detail: `Doctor updated with doctorId: ${doctor.doctorId}`,
          };
          this._listDoctorService.setChangeMessage(message);
          return this._listDoctorService.list();
        })
      )
      .subscribe((doctors: Doctor[]) => {
        this._listDoctorService.setChangeDoctor(doctors);
      });
  }

  private _instanceCreateDoctor(): DoctorCreateInput {
    let instance: DoctorCreateInput = {
      names: this.doctor.names,
      lastName: this.doctor.lastName,
      cmp: this.doctor.cmp,
      urlPhoto: this.doctor.urlPhoto,
    };

    return instance;
  }

  private _instanceUpdateDoctor(): DoctorInput {
    let instance: DoctorInput = {
      doctorId: this.doctor.doctorId,
      names: this.doctor.names,
      lastName: this.doctor.lastName,
      urlPhoto: this.doctor.urlPhoto,
    };

    return instance;
  }
}
