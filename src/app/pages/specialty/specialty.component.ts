import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Message, MessageService } from 'primeng/api';
import { Subject, switchMap, take, takeUntil } from 'rxjs';
import { Specialty } from 'src/app/responses/specialty.response';
import { DeleteByIdSpecialtiesService } from 'src/app/service/delete-by-id-specialties.service';
import { ListSpecialtiesService } from 'src/app/service/list-specialties.service';

@Component({
  providers: [MessageService],
  selector: 'app-specialty',
  templateUrl: './specialty.component.html',
  styleUrls: ['./specialty.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SpecialtyComponent implements OnInit, OnDestroy {
  public columns: any;
  public cols: any[];
  public data: Specialty[];
  public first: number;
  public loading: boolean;

  private _unSubscribeSubject: Subject<void>;

  constructor(
    private _deleteSpecialtyService: DeleteByIdSpecialtiesService,
    private _listSpecialtiesService: ListSpecialtiesService,
    private _messageService: MessageService,
    private _cdr: ChangeDetectorRef
  ) {
    this.cols = [
      { field: 'specialtyId', header: 'SpecialtyId' },
      { field: 'name', header: 'Name' },
      { field: 'description', header: 'Description' },
      { field: 'actions', header: 'Actions' },
    ];
    this._unSubscribeSubject = new Subject<void>();
    this.loading = true;
    this.first = 0;
    this.data = [];
  }

  public ngOnInit(): void {
    this._initialize();
  }

  public ngOnDestroy(): void {
    this._finalize();
  }

  public applyFilterGlobal(event: any): string {
    return event.target.value;
  }

  public deleteSpecialty(id: number): void {
    this._deleteSpecialtyService
      .delete(id)
      .pipe(
        switchMap(() => {
          const message: Message = {
            severity: 'success',
            summary: 'Deleted successfully',
            detail: `Specialty removed with id: ${id}`,
          };
          this._createToast(message);
          return this._listSpecialtiesService.list();
        }),
        take(1)
      )
      .subscribe((specialties: Specialty[]) => {
        this._createTable(specialties);
      });
  }

  private _initialize(): void {
    this._loadSpecialities();
    this._watchChangeSpecialities();
    this._watchChangeMessage();
    this.loading = false;
  }

  private _finalize(): void {
    this._unSubscribeSubject.next();
    this._unSubscribeSubject.complete();
  }

  private _loadSpecialities(): void {
    this._listSpecialtiesService
      .list()
      .pipe(take(1))
      .subscribe((specialties: Specialty[]) => {
        this._createTable(specialties);
      });
  }

  private _watchChangeSpecialities(): void {
    this._listSpecialtiesService
      .getchangeSpecialities()
      .pipe(takeUntil(this._unSubscribeSubject))
      .subscribe((specialties: Specialty[]) => {
        this._createTable(specialties);
      });
  }

  private _watchChangeMessage(): void {
    this._listSpecialtiesService
      .getchangeMessage()
      .pipe(takeUntil(this._unSubscribeSubject))
      .subscribe((message: Message) => {
        this._createToast(message);
      });
  }

  private _createToast(message: Message): void {
    this._messageService.add(message);
    this._cdr.markForCheck();
  }

  private _createTable(specialties: Specialty[]): void {
    this.data = specialties;
    this._cdr.markForCheck();
  }
}
