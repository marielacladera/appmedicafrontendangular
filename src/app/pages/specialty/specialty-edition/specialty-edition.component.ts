import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Message } from 'primeng/api';
import { switchMap, take } from 'rxjs';
import { SpecialtyInput } from 'src/app/inputs/specialty.input';
import { SpecialtyCreateInput } from 'src/app/inputs/specialty-create.input';
import { Specialty } from 'src/app/responses/specialty.response';
import { CreateSpecialtyService } from 'src/app/service/create-specialty.service';
import { ListByIdSpecialtyService } from 'src/app/service/list-by-id-specialties.service';
import { ListSpecialtiesService } from 'src/app/service/list-specialties.service';
import { UpdateSpecialtyService } from 'src/app/service/update-specialty.service';

@Component({
  selector: 'app-specialty-edition',
  templateUrl: './specialty-edition.component.html',
  styleUrls: ['./specialty-edition.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SpecialtyEditionComponent implements AfterViewInit {
  public edit: boolean;
  public form: FormGroup;
  public id: number;

  constructor(
    private _listByIdSpecialtyService: ListByIdSpecialtyService,
    private _createSpecialtyService: CreateSpecialtyService,
    private _listSpecialtiesService: ListSpecialtiesService,
    private _updateSpecialtyService: UpdateSpecialtyService,
    private _cdr: ChangeDetectorRef,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.form = new FormGroup({
      specialtyId: new FormControl('0'),
      name: new FormControl(''),
      description: new FormControl(''),
    });
    this.edit = false;
    this.id = 0;
  }

  public ngAfterViewInit(): void {
    this._initializeForm();
  }

  public operate(): void {
    this.edit ? this._specialtyUpdate() : this._specialtyCreate();
    this._router.navigate(['specialties']);
  }

  private _initializeForm(): void {
    this._route.params.pipe(take(1)).subscribe((data) => {
      this.id = data['id'];
      this.edit = data['id'] != null;
      this._loadForm();
    });
  }

  private _loadForm(): void {
    if (this.edit) {
      this._listByIdSpecialtyService
        .listById(this.id)
        .pipe(take(1))
        .subscribe((specialty: Specialty) => {
          this._changeValuesForm(specialty);
        });
    }
  }

  private _changeValuesForm(specialty: Specialty): void {
    this.form = new FormGroup({
      specialtyId: new FormControl(specialty.specialtyId),
      name: new FormControl(specialty.name),
      description: new FormControl(specialty.description),
    });
    this._cdr.markForCheck();
  }

  private _specialtyUpdate(): void {
    if (this._isFormValid()) {
      this._updateSpecialtyService
        .update(this._createInstanceSpecialtyUpdate())
        .pipe(
          switchMap((specialty: Specialty) => {
            const message: Message = {
              severity: 'success',
              summary: 'Success',
              detail: `Specialty updated with specialtyId: ${specialty.specialtyId}`,
            };
            this._listSpecialtiesService.setChangeMessage(message);
            return this._listSpecialtiesService.list();
          }),
          take(1)
        )
        .subscribe((specialties: Specialty[]) => {
          this._listSpecialtiesService.setChangeSpecialities(specialties);
        });
    }
  }

  private _specialtyCreate(): void {
    if (this._isFormValid()) {
      this._createSpecialtyService
        .create(this._createInstanceSpecialtyCreate())
        .pipe(
          switchMap((specialty: Specialty) => {
            const message: Message = {
              severity: 'success',
              summary: 'Success',
              detail: `Specialty created with specialtyId: ${specialty.specialtyId}`,
            };
            this._listSpecialtiesService.setChangeMessage(message);
            return this._listSpecialtiesService.list();
          }),
          take(1)
        )
        .subscribe((specialties: Specialty[]) => {
          this._listSpecialtiesService.setChangeSpecialities(specialties);
        });
    }
  }

  private _isFormValid(): boolean {
    return this.form.value['name'] && this.form.value['description'];
  }

  private _createInstanceSpecialtyUpdate(): SpecialtyInput {
    const instance: SpecialtyInput = {
      specialtyId: this.form.value['specialtyId'],
      name: this.form.value['name'],
      description: this.form.value['description'],
    };

    return instance;
  }

  private _createInstanceSpecialtyCreate(): SpecialtyCreateInput {
    const instance: SpecialtyCreateInput = {
      name: this.form.value['name'],
      description: this.form.value['description'],
    };

    return instance;
  }
}
