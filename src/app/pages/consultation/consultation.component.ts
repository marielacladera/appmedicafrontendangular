import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { take } from 'rxjs';
import { ConsultationCreateInput } from 'src/app/inputs/consultation-create.input';
import { ConsultationListExamCreateInput } from 'src/app/inputs/consultation-list-exam-create.input';
import { DetailConsultationInput } from 'src/app/inputs/detail-consultation.input';
import { Doctor } from 'src/app/responses/doctor.response';
import { Exam } from 'src/app/responses/exam.response';
import { Patient } from 'src/app/responses/patient.response';
import { Specialty } from 'src/app/responses/specialty.response';
import { CreateConsultationService } from 'src/app/service/create-consultation.service';
import { ListByIdDoctorService } from 'src/app/service/list-by-id-doctor.service';
import { ListByIdPatientService } from 'src/app/service/list-by-id-patient.service';
import { ListDoctorsService } from 'src/app/service/list-doctors.service';
import { ListExamsService } from 'src/app/service/list-exams.service';
import { ListPatientsService } from 'src/app/service/list-patients.service';
import { ListSpecialtiesService } from 'src/app/service/list-specialties.service';
import { DoctorWrapper } from 'src/app/wrappers/doctor.wrapper';
import { PatientWrapper } from 'src/app/wrappers/patient.wrapper';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConsultationComponent implements OnInit {
  public consultory!: string;
  public consultories: string[];
  public date!: Date;
  public detailConsultations: DetailConsultationInput[];
  public diagnosis!: string;
  public doctors: DoctorWrapper[];
  public doctorWrapper!: DoctorWrapper;
  public exam!: Exam;
  public exams: Exam[];
  public examsSelects: Exam[];
  public minDate: Date;
  public patients: PatientWrapper[];
  public patientWrapper!: PatientWrapper;
  public specialties: Specialty[];
  public specialty!: Specialty;
  public treatment!: string;
  private _patient: Patient;
  private _doctor: Doctor;

  constructor(
    private _createConsultationService: CreateConsultationService,
    private _listPatientByIdService: ListByIdPatientService,
    private _specialtyListService: ListSpecialtiesService,
    private _listDoctorByIdService: ListByIdDoctorService,
    private _patientListService: ListPatientsService,
    private _doctorListService: ListDoctorsService,
    private _examListService: ListExamsService,
    private _cdr: ChangeDetectorRef
  ) {
    this._patient = {
      patientId: 0,
      names: '',
      lastName: '',
      dni: '',
      address: '',
      phone: '',
      email: '',
    };
    this._doctor = {
      doctorId: 0,
      names: '',
      lastName: '',
      cmp: '',
      urlPhoto: '',
    };
    this.consultories = ['C1', 'C2', 'C3', 'C4'];
    this.detailConsultations = [];
    this.minDate = new Date();
    this.examsSelects = [];
    this.specialties = [];
    this.patients = [];
    this.doctors = [];
    this.exams = [];
  }

  ngOnInit(): void {
    this._initialize();
  }

  public registerDetail(): void {
    if (this._isDetailvalid()) {
      const detail: DetailConsultationInput = {
        diagnosis: this.diagnosis,
        treatment: this.treatment,
      };
      this.detailConsultations.push(detail);
      this._clearDetailConsultation();
    }
  }

  public removeDetail(pos: number): void {
    this.detailConsultations.splice(pos, 1);
  }

  public registerExam(): void {
    if (this.exam != null) {
      let exist: boolean = false;
      for (let i = 0; i < this.examsSelects.length; i++) {
        if (this.examsSelects[i] === this.exam) {
          exist = true;
          break;
        }
      }
      if (exist == false) {
        this.examsSelects.push(this.exam);
      }
    }
  }

  public removeExam(pos: number): void {
    this.examsSelects.splice(pos, 1);
  }

  public operate(): void {
    if (this._isValid()) {
      this._registerConsultation();
    } else {
      console.log(this._isValid);
    }
  }

  public onDoctorChange(): void {
    this._createInstanceDoctor();
  }

  public onPatientChange(): void {
    this._createInstancePatient();
  }

  private _initialize(): void {
    this._loadListSpecialties();
    this._loadListPatients();
    this._loadListDoctor();
    this._loadListExam();
  }

  private _loadListDoctor(): void {
    this._doctorListService.list().subscribe((doctors: Doctor[]) => {
      this.doctors = this._listDoctors(doctors);
      this._cdr.markForCheck();
    });
  }

  private _loadListPatients(): void {
    this._patientListService.list().subscribe((patients: Patient[]) => {
      this.patients = this._listPatients(patients);
      this._cdr.markForCheck();
    });
  }

  private _loadListSpecialties(): void {
    this._specialtyListService.list().subscribe((specialties: Specialty[]) => {
      this.specialties = specialties;
      this._cdr.markForCheck();
    });
  }

  private _loadListExam(): void {
    this._examListService.list().subscribe((exams: Exam[]) => {
      this.exams = exams;
      this._cdr.markForCheck();
    });
  }

  private _listDoctors(doctors: Doctor[]): DoctorWrapper[] {
    const auxiliarList: DoctorWrapper[] = [];
    doctors.forEach((doctor) => {
      auxiliarList.push(this._instanceDoctorWrapper(doctor));
    });

    return auxiliarList;
  }

  private _listPatients(patients: Patient[]): PatientWrapper[] {
    const auxiliarList: PatientWrapper[] = [];
    patients.forEach((patient) => {
      auxiliarList.push(this._instancePacienteWrapper(patient));
    });

    return auxiliarList;
  }

  private _instanceDoctorWrapper(doctor: Doctor): DoctorWrapper {
    let instance: DoctorWrapper = {
      doctorId: doctor.doctorId,
      fullName: `${doctor.names} ${doctor.lastName}`,
    };

    return instance;
  }

  private _instancePacienteWrapper(patient: Patient): PatientWrapper {
    let instance: PatientWrapper = {
      patientId: patient.patientId,
      fullName: `${patient.names} ${patient.lastName}`,
    };

    return instance;
  }

  private _isDetailvalid(): boolean {
    return !!this.diagnosis.length && !!this.treatment.length;
  }

  private _clearDetailConsultation(): void {
    this.diagnosis = '';
    this.treatment = '';
  }

  private _registerConsultation() {
    const consultation: ConsultationCreateInput =
      this._createIntanceConsultation();
    const consultationListExams: ConsultationListExamCreateInput = {
      consultation: consultation,
      exams: this.examsSelects,
    };
    this._createConsultationService
      .create(consultationListExams)
      .pipe(take(1))
      .subscribe(() => {});
  }

  private _createIntanceConsultation(): ConsultationCreateInput {
    let instance: ConsultationCreateInput = {
      doctor: this._doctor,
      patient: this._patient,
      specialty: this.specialty,
      date: this._formatTOISODate(),
      officeNumber: this.consultory,
      detailsConsultation: this.detailConsultations,
    };
    return instance;
  }

  private _createInstancePatient(): void {
    this._listPatientByIdService
      .listById(this.patientWrapper.patientId)
      .pipe(take(1))
      .subscribe((patient: Patient) => {
        this._patient = patient;
      });
  }

  private _createInstanceDoctor(): void {
    this._listDoctorByIdService
      .listById(this.doctorWrapper.doctorId)
      .pipe(take(1))
      .subscribe((doctor: Doctor) => {
        this._doctor = doctor;
      });
  }

  private _formatTOISODate(): string {
    const tzoffset = new Date().getTimezoneOffset() * 60000;
    const localISOTime: string = new Date(
      this.date.getTime() - tzoffset
    ).toISOString();
    return localISOTime;
  }

  private _format(): string {
    const ISOdate = this.date.toISOString();
    const hour = this.date.toLocaleTimeString();
    return ISOdate.slice(0, 10) + 'T' + hour;
  }

  private _isValid(): boolean {
    return (
      !!this.consultory &&
      !!this.date &&
      !!this.detailConsultations &&
      this.doctorWrapper !== undefined &&
      !!this.examsSelects &&
      this.patientWrapper !== undefined &&
      this.specialty !== undefined
    );
  }
}
