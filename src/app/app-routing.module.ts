import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultationComponent } from './pages/consultation/consultation.component';
import { DoctorComponent } from './pages/doctor/doctor.component';
import { ExamEditionComponent } from './pages/exam/exam-edition/exam-edition.component';
import { ExamComponent } from './pages/exam/exam.component';
import { PatientEditionComponent } from './pages/patient/patient-edition/patient-edition.component';
import { PatientComponent } from './pages/patient/patient.component';
import { SpecialtyEditionComponent } from './pages/specialty/specialty-edition/specialty-edition.component';
import { SpecialtyComponent } from './pages/specialty/specialty.component';

const routes: Routes = [
  { path: 'register', component: ConsultationComponent},
  { path: 'doctors', component: DoctorComponent },
  { path: 'exams', component: ExamComponent, children: [
      { path: 'new', component: ExamEditionComponent },
      { path: 'edit/:id', component: ExamEditionComponent }
    ]
  },
  { path: 'patients', component: PatientComponent, children: [
      { path: 'new', component: PatientEditionComponent },
      { path: 'edit/:id', component: PatientEditionComponent },
    ]
  },
  { path: 'specialties', component: SpecialtyComponent, children: [
      { path: 'new', component:SpecialtyEditionComponent },
      { path: 'edit/:id', component: SpecialtyEditionComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
