import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlDoctors } from '../constants/constants';
import { DoctorInput } from '../inputs/doctor.input';
import { Doctor } from '../responses/doctor.response';

@Injectable({
  providedIn: 'root'
})
export class UpdateDoctorService {

  constructor(
    private _http: HttpClient
  ) { }

  public update(doctor: DoctorInput): Observable<Doctor> {
    return this._http.put<Doctor>(urlDoctors, doctor);
  }
}
