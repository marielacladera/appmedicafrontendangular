import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlExams } from '../constants/constants';
import { ExamCreateInput } from '../inputs/exam-create.input';
import { Exam } from '../responses/exam.response';

@Injectable({
  providedIn: 'root'
})
export class CreateExamService {

  constructor(
    private _http: HttpClient,
  ) { }

  public create(exam: ExamCreateInput): Observable<Exam> {
    return this._http.post<Exam>(urlExams, exam);
  }
}
