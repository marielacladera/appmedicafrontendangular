import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlPatients } from '../constants/constants';
import { PatientInput } from '../inputs/patient.input';
import { Patient } from '../responses/patient.response';

@Injectable({
  providedIn: 'root'
})
export class UpdatePatientService {

  constructor(
    private _http: HttpClient,
  ) { }

  public update(patient: PatientInput): Observable<Patient> {
    return this._http.put<Patient>(urlPatients, patient);
  }
}
