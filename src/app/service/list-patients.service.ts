import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from 'primeng/api';
import { Observable, Subject } from 'rxjs';
import { urlPatients } from '../constants/constants';
import { Patient } from '../responses/patient.response';

@Injectable({
  providedIn: 'root',
})
export class ListPatientsService {
  private _changePatient = new Subject<Patient[]>();
  private _changeMessage = new Subject<Message>();
  constructor(
    private _http: HttpClient) {}

  public list(): Observable<Patient[]> {
    return this._http.get<Patient[]>(urlPatients);
  }

  public getChangePatient(): Observable<Patient[]> {
    return this._changePatient.asObservable();
  }

  public setChangePatient(patients: Patient[]): void {
    this._changePatient.next(patients);
  }

  public getChangeMessage(): Observable<Message> {
    return this._changeMessage.asObservable();
  }

  public setChangeMessage(message: Message): void {
    this._changeMessage.next(message);
  }
}
