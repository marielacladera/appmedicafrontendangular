import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlSpecialties } from '../constants/constants';
import { SpecialtyCreateInput } from '../inputs/specialty-create.input';
import { Specialty } from '../responses/specialty.response';

@Injectable({
  providedIn: 'root'
})
export class CreateSpecialtyService {

  constructor(
    private _http: HttpClient,
  ) { }

  public create(specialty: SpecialtyCreateInput): Observable<Specialty> {
    return this._http.post<Specialty>(urlSpecialties, specialty);
  }
}
