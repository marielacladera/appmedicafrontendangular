import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlConsultation } from '../constants/constants';
import { ConsultationListExamCreateInput } from '../inputs/consultation-list-exam-create.input';

@Injectable({
  providedIn: 'root'
})
export class CreateConsultationService {

  constructor(
    private _http: HttpClient,
  ) { }

  public create(consultationList: ConsultationListExamCreateInput): Observable<void> {
    return this._http.post<void>(urlConsultation, consultationList);
  }
}
