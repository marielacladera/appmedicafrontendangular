import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlSpecialties } from '../constants/constants';
import { SpecialtyInput } from '../inputs/specialty.input';
import { Specialty } from '../responses/specialty.response';

@Injectable({
  providedIn: 'root'
})
export class UpdateSpecialtyService {

  constructor(
    private _http: HttpClient,
  ) { }

  public update(specialty: SpecialtyInput): Observable<Specialty> {
    return this._http.put<Specialty>(urlSpecialties, specialty);
  }
}
