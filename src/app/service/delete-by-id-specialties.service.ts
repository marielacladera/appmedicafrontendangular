import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlSpecialties } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class DeleteByIdSpecialtiesService {

  constructor(
    private _http: HttpClient
  ) { }

  public delete(id: number): Observable<void> {
    return this._http.delete<void>(`${urlSpecialties}/${id}`);
  }
}
