import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlDoctors } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class DeleteByIdDoctorService {

  constructor(
    private _http: HttpClient,
  ) { }

  public delete(id: number): Observable<void> {
    return this._http.delete<void>(`${urlDoctors}/${id}`);
  }
}
