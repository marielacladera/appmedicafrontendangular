import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlPatients } from '../constants/constants';
import { Patient } from '../responses/patient.response';

@Injectable({
  providedIn: 'root'
})
export class ListByIdPatientService {

  constructor(
    private _http: HttpClient,
  ) { }

  public listById(id: number): Observable<Patient> {
    return this._http.get<Patient>(`${urlPatients}/${id}`);
  }
}
