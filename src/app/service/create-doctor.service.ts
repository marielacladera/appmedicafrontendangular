import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlDoctors } from '../constants/constants';
import { DoctorCreateInput } from '../inputs/doctor-create.input';
import { Doctor } from '../responses/doctor.response';

@Injectable({
  providedIn: 'root'
})
export class CreateDoctorService {

  constructor(
    private _http: HttpClient
  ) { }

  public create(doctor: DoctorCreateInput): Observable<Doctor> {
    return this._http.post<Doctor>(urlDoctors, doctor);
  }
}
