import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlExams } from '../constants/constants';
import { ExamInput } from '../inputs/exam.input';
import { Exam } from '../responses/exam.response';

@Injectable({
  providedIn: 'root'
})
export class UpdateExamService {

  constructor(
    private _http: HttpClient,
  ) { }

  public update(exam: ExamInput): Observable<Exam> {
    return this._http.put<Exam>(urlExams, exam);
  }
}
