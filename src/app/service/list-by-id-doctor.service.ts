import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlDoctors } from '../constants/constants';
import { Doctor } from '../responses/doctor.response';

@Injectable({
  providedIn: 'root'
})
export class ListByIdDoctorService {

  constructor(
    private _http: HttpClient
  ) { }

  public listById(id: number): Observable<Doctor> {
    return this._http.get<Doctor>(`${urlDoctors}/${id}`);
  }
}
