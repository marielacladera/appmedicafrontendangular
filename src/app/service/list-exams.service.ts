import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from 'primeng/api';
import { Observable, Subject } from 'rxjs';
import { urlExams } from '../constants/constants';
import { Exam } from '../responses/exam.response';

@Injectable({
  providedIn: 'root'
})
export class ListExamsService {

  private _changeExams: Subject<Exam []>;
  private _changeMessage: Subject<Message>;

  constructor(
    private _http: HttpClient
  ) {
    this._changeExams = new Subject<Exam []>();
    this._changeMessage = new Subject<Message>();
  }

  public list(): Observable<Exam[]>{
    return this._http.get<Exam[]>(urlExams);
  }

  public getchangeExam(): Observable<Exam[]> {
    return this._changeExams.asObservable();
  }

  public setChangeExams(exams: Exam[]): void {
    this._changeExams.next(exams);
  }

  public getchangeMessage(): Observable<Message> {
    return this._changeMessage.asObservable();
  }

  public setChangeMessage(message: Message): void {
    this._changeMessage.next(message);
  }
}
