import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlPatients } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class DeleteByIdPatientService {

  constructor(
    private _http: HttpClient,
  ) { }

  public delete(id: number): Observable<void> {
    return this._http.delete<void>(`${urlPatients}/${id}`);
  }
}
