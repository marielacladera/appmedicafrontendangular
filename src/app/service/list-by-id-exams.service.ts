import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlExams } from '../constants/constants';
import { Exam } from '../responses/exam.response';

@Injectable({
  providedIn: 'root'
})
export class ListByIdExamsService {

  constructor(
    private _http: HttpClient
  ) { }

  public listById(id: number): Observable<Exam> {
    return this._http.get<Exam>(`${urlExams}/${id}`);
  }
}
