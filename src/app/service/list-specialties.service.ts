import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from 'primeng/api';
import { Observable, Subject } from 'rxjs';
import { urlSpecialties } from '../constants/constants';
import { Specialty } from '../responses/specialty.response';

@Injectable({
  providedIn: 'root'
})
export class ListSpecialtiesService {

  private _changeSpecialty : Subject<Specialty []>;
  private _changeMessage: Subject<Message>;

  constructor(
    private _http: HttpClient
  ) {
    this._changeSpecialty = new Subject<Specialty []>();
    this._changeMessage = new Subject<Message>();
  }

  public list(): Observable<Specialty[]> {
    return this._http.get<Specialty[]>(urlSpecialties);
  }

  public getchangeSpecialities(): Observable<Specialty[]> {
    return this._changeSpecialty.asObservable();
  }

  public setChangeSpecialities(specialities: Specialty[]): void {
    this._changeSpecialty.next(specialities);
  }

  public getchangeMessage(): Observable<Message> {
    return this._changeMessage.asObservable();
  }

  public setChangeMessage(message: Message): void {
    this._changeMessage.next(message);
  }
}
