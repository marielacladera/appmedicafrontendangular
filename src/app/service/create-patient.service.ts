import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlPatients } from '../constants/constants';
import { PatientCreateInput } from '../inputs/patient-create.input';
import { Patient } from '../responses/patient.response';

@Injectable({
  providedIn: 'root'
})
export class CreatePatientService {

  constructor(
    private _http: HttpClient
  ) { }

  public create(patient: PatientCreateInput): Observable<Patient> {
    return this._http.post<Patient>(urlPatients, patient);
  }
}
