import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { urlSpecialties } from '../constants/constants';
import { Specialty } from '../responses/specialty.response';

@Injectable({
  providedIn: 'root'
})
export class ListByIdSpecialtyService {

  constructor(
    private _http: HttpClient,
  ) { }

  public listById(id: number): Observable<Specialty> {
    return this._http.get<Specialty>(`${urlSpecialties}/${id}`);
  }
}
