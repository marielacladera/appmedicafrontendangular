import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from 'primeng/api';
import { Observable, Subject } from 'rxjs';
import { urlDoctors } from '../constants/constants';
import { Doctor } from '../responses/doctor.response';

@Injectable({
  providedIn: 'root'
})
export class ListDoctorsService {

  private _changeDoctor: Subject<Doctor[]>;
  private _changeMessage: Subject<Message>;

  constructor(
    private _http: HttpClient,
  ) {
    this._changeDoctor = new Subject<Doctor[]>();
    this._changeMessage = new Subject<Message>();
  }

  public list() : Observable<Doctor[]> {
    return this._http.get<Doctor[]>(urlDoctors);
  }

  public getChangeDoctor(): Observable<Doctor[]> {
    return this._changeDoctor.asObservable();
  }

  public setChangeDoctor(doctors: Doctor[]): void {
    this._changeDoctor.next(doctors);
  }

  public getChangeMessage(): Observable<Message> {
    return this._changeMessage.asObservable();
  }

  public setChangeMessage(message: Message): void {
    this._changeMessage.next(message);
  }
}
