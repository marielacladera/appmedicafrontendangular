import { environment } from "src/environments/environment";

export const urlConsultation = `${environment.HOST}/consultations`

export const urlPatients =  `${environment.HOST}/patients`;

export const urlDoctors = `${environment.HOST}/doctors`;

export const urlExams = `${environment.HOST}/exams`;

export const urlSpecialties = `${environment.HOST}/specialties`;
