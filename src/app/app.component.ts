import { Component } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})


export class AppComponent {
  title = 'mediapp-prime';
  visibleSidebar = false;

  items: MenuItem[] = [{
    icon: 'pi pi-ellipsis-v',
    items: [
      {label: 'Perfil', icon: 'pi pi-user'},
      {separator: true},
      {label: 'Cerrar', icon: 'pi pi-fw pi-power-off'}
    ]
  }]
}
