import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrimeModule } from './prime/prime.module';
import { HttpClientModule } from '@angular/common/http';

import { PatientComponent } from './pages/patient/patient.component';
import { DoctorComponent } from './pages/doctor/doctor.component';
import { ExamComponent } from './pages/exam/exam.component';import { PatientEditionComponent } from './pages/patient/patient-edition/patient-edition.component';
import { DialogDoctorComponent } from './pages/doctor/dialog-doctor/dialog-doctor.component';
import { ExamEditionComponent } from './pages/exam/exam-edition/exam-edition.component';
import { SpecialtyComponent } from './pages/specialty/specialty.component';
import { SpecialtyEditionComponent } from './pages/specialty/specialty-edition/specialty-edition.component';
import { ConsultationComponent } from './pages/consultation/consultation.component';

@NgModule({
  declarations: [
    AppComponent,
    PatientComponent,
    DoctorComponent,
    ExamComponent,
    PatientEditionComponent,
    DialogDoctorComponent,
    ExamEditionComponent,
    SpecialtyComponent,
    SpecialtyEditionComponent,
    ConsultationComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    PrimeModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
