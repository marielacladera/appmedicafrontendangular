export interface DetailConsultationInput {
  diagnosis: string,
  treatment: string
}
