export interface PatientInput {
  patientId: number;
  names: string;
  lastName: string;
  dni: string;
  address: string;
  phone: string;
  email: string;
}
