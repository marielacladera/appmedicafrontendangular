export interface ExamInput {

  examId: number;
  name: string;
  description: string;

}
