export interface DoctorInput {

  doctorId: number;
  names: string;
  lastName: string;
  urlPhoto: string;

}
