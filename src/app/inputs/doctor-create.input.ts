export interface DoctorCreateInput {

  names: string;
  lastName: string;
  cmp: string;
  urlPhoto: string;

}
