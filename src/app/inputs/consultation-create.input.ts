import { Doctor } from "../responses/doctor.response";
import { Patient } from "../responses/patient.response";
import { Specialty } from "../responses/specialty.response";
import { DetailConsultationInput } from "./detail-consultation.input";

export interface ConsultationCreateInput {

  doctor: Doctor,
  patient: Patient,
  specialty: Specialty,
  date: string,
  officeNumber: string,
  detailsConsultation: DetailConsultationInput[]

}
