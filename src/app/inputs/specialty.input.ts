export interface SpecialtyInput{

  specialtyId: number;
  name: string;
  description: string;

}
