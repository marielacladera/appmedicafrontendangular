export interface SpecialtyCreateInput {

  name: string;
  description: string;

}
