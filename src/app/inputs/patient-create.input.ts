export interface PatientCreateInput {
  names: string;
  lastName: string;
  dni: string;
  address: string;
  phone: string;
  email: string;
}
