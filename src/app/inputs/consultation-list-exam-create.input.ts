import { Exam } from "../responses/exam.response";
import { ConsultationCreateInput } from "./consultation-create.input";

export interface ConsultationListExamCreateInput{

  consultation: ConsultationCreateInput,
  exams: Exam[]

}
