export interface Exam {
  examId: number,
  name: string,
  description: string
}
