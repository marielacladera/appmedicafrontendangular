export interface Specialty {
  specialtyId: number,
  name: string,
  description: string
}
