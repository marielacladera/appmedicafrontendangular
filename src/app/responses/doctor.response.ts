export interface Doctor{
  doctorId: number,
  names: string,
  lastName: string,
  cmp: string,
  urlPhoto: string
}
