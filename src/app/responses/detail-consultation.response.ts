export interface DetailConsultation {
  diagnosis: string,
  treatment: string
}
